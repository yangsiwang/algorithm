package com.mlamp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }


    @Test
    public void charCount() {
        String input = "hello world";
        char[] chars = input.toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        int res = Integer.MIN_VALUE;
        char target = input.charAt(0);
        for (char c : chars) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
            if (map.get(c) > res){
                res = map.get(c);
                target = c;
            }
        }
        System.out.println(target);
        System.out.println(res);
    }
}
