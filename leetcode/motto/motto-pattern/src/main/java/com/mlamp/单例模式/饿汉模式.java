package com.mlamp.单例模式;

public class 饿汉模式 {

    private static final class Singleton {
        private static Singleton INSTANCE = new Singleton();

        public static final Singleton getInstance() {
            return INSTANCE;
        }
    }
}
