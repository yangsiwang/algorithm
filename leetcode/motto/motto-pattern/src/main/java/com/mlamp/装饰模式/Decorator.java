/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:Decorator.java
 * Date:2021/02/02 14:16:02
 */

package com.mlamp.装饰模式;


// 给对象动态增加新功能，需要持有对象的实例：装饰器模式就是给一个对象增加一些新功能，而且是动态的，要求装饰对象和被装饰对象实现同一个接口，装饰对象
// 只有被装饰对象的实例



//适用场景： 1. 需要扩展一个类； 2. 动态的为一个对象增加功能，而且还能动态的撤销。（继承做不到这一点，继承的功能是静态的，不能动态删除）。
public class Decorator {

    public static interface Sourceable {
        public void method();
    }

    //被装饰类
    public static class Source implements Sourceable {

        @Override
        public void method() {
            System.out.println("the original method");
        }
    }

    //装饰类
    public static class DecoratorClazz implements Sourceable {

        private Sourceable source;


        public DecoratorClazz(Sourceable source) {
            super();
            this.source = source;
        }


        @Override
        public void method() {
            System.out.println("before decorator");
            source.method();
            System.out.println("after decorator");
        }
    }

}
