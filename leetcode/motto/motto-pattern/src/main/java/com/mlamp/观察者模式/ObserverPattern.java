/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:ObserverPattern.java
 * Date:2021/02/02 18:15:02
 */

package com.mlamp.观察者模式;


//观察者模式也就是（发布-订阅模式）
//当一个对象变化时，其他依赖该对象的对象都会受到通知，并且随着变化，对象之间是一种一对多的关系。类似于邮件订阅和RSS订阅，当你订阅了改文章，如果后续有更新，会及时通知你。
public class ObserverPattern {


}
