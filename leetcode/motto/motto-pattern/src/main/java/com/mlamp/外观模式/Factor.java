/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:Factor.java
 * Date:2021/02/02 14:26:02
 */

package com.mlamp.外观模式;


// 集成所有操作到一个类：外观模式是为了解决类与类之间有依赖关系的，比如spring，可以将类与类之间的关系配置到配置文件中，而外观模式将他们之间的关系放在
// 一个facade中，降低类类之间的耦合
public class Factor {


    public static interface Action {
        public void startup();

        public void shutdow();
    }

    private static class CPU implements Action {
        @Override
        public void startup() {
            System.out.println("cpu startup");
        }

        @Override
        public void shutdow() {
            System.out.println("cpu shutdown");
        }
    }

    private static class Memory implements Action {

        @Override
        public void startup() {
            System.out.println("memory startup");
        }

        @Override
        public void shutdow() {
            System.out.println("memory shutdown");
        }
    }

    private static class Disk implements Action {

        @Override
        public void startup() {
            System.out.println("disk startup");
        }

        @Override
        public void shutdow() {
            System.out.println("disk shutdown");
        }
    }

    public static class Computer implements Action {

        private CPU cpu;
        private Memory memory;
        private Disk disk;


        public Computer() {

            cpu = new CPU();
            memory = new Memory();
            disk = new Disk();

        }


        @Override
        public void startup() {
            System.out.println("start the computer");
            cpu.startup();
            memory.startup();
            disk.startup();
            System.out.println("start computer finished");
        }

        @Override
        public void shutdow() {
            System.out.println("begin to close the computer");
            cpu.shutdow();
            memory.shutdow();
            disk.shutdow();
            System.out.println("computer closed");
        }
    }
}
