package com.mlamp.原型模式;

import java.util.Objects;

public class Prototype implements Cloneable {

    private String field1;
    private String field2;
    private String field3;


    public Prototype() {

    }

    public Prototype(String field1, String field2, String field3) {
        this.field1 = field1;
        this.field2 = field2;
        this.field3 = field3;
    }


    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prototype prototype = (Prototype) o;
        return Objects.equals(field1, prototype.field1) &&
                Objects.equals(field2, prototype.field2) &&
                Objects.equals(field3, prototype.field3);
    }

    @Override
    public String toString() {
        return "Prototype{" +
                "field1='" + field1 + '\'' +
                ", field2='" + field2 + '\'' +
                ", field3='" + field3 + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(field1, field2, field3);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Prototype prototype = null;
        try {
            prototype = (Prototype) super.clone();
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return prototype;
    }


    public static void main(String[] args) {
        Prototype instance1 = new Prototype("f1", "f2", "f3");
        try {
            Prototype instance2 = (Prototype) instance1.clone();
            System.out.println(instance2.toString());
            instance1.setField1("field1");
            System.out.println(instance2.toString());

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }


    }
}
