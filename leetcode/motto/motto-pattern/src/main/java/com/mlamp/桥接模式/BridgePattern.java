/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:BridgePattern.java
 * Date:2021/02/02 14:33:02
 */

package com.mlamp.桥接模式;


//桥接模式就是把事物和其具体实现分开，使他们可以各自独立的变化。
//将抽象和实现解耦，二者可以独立变化，像我们蝉蛹的jdbc和drivermanager一样，jdbc进行连接数据库的时候，在各个数据库之间进行切换，基本不需要动太多的代码，
//甚至丝毫不用动，原因就是jdbc提供统一接口，每个数据库提供各自的实现，用一个叫做数据库驱动的程序桥接就可以了。
public class BridgePattern {
}
