package com.mlamp;

public class VolatileTest {

    private static volatile int race = 0;

    public static void increment() {
        race++;
    }

    public static final int THREAD_NUM = 20;

    public static void main(String[] args) {

        Thread[] threads = new Thread[THREAD_NUM];
        for (int i = 0; i < THREAD_NUM; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 10000; j++) {
                        increment();
                    }
                }
            });
            threads[i].start();
        }
        while (Thread.activeCount() > 1) Thread.yield();
        System.out.println(race);

    }
}
