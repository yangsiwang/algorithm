package com.mlamp;

public class 回文数判断 {

    public static boolean isPalindrome(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div /= 100;
        }
        return true;
    }


    /**
     * 快速判断回文数
     *
     * @param x
     * @return
     */

    public static boolean isPalindromeC(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div /= 100;
        }
        return true;
    }


    public static boolean isPalindromeD(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div /= 100;
        }
        return true;
    }


    public static boolean isPalindromeA(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div = div / 100;
        }
        return true;

    }


    public static boolean isPalindromeB(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div /= 100;
        }
        return true;
    }


    public static boolean isPalindrome4(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div = div / 100;
        }
        return true;
    }


    public static boolean isPalindrome5(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div = div / 100;
        }
        return true;
    }


    public static boolean isPalindrome3(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div = div / 100;
        }
        return true;
    }


    public static boolean isPalindrome2(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) {
            div *= 10;
        }
        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = x % div / 10;
            div = div / 100;
        }
        return true;
    }

    public static void main(String[] args) {
        int num1 = 121;
        System.out.println(isPalindrome(num1));
    }
}
