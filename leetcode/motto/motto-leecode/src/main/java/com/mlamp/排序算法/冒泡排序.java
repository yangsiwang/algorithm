package com.mlamp.排序算法;

import java.util.Arrays;

public class 冒泡排序 {

    public static void main(String[] args) {

        int[] ints = 基础排序.generateArray(30);
        bubblesort(ints);
        System.out.println(Arrays.toString(ints));

    }


    public static  void bubblesort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j + 1] < array[j]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }


}
