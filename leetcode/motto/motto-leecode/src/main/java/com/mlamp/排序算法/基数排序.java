package com.mlamp.排序算法;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class 基数排序 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        int[] ints1 = radixSort(ints);
        System.out.println(Arrays.toString(ints1));

    }

    public static int[] radixSort(int[] array) {
        if (array == null || array.length < 2) return array;
            int max = array[0];
            for (int i = 0; i < array.length; i++) {
                if (array[i] > max) max = array[i];
            }
            int maxDigit = 0;
            while (max != 0) {
                max = max / 10;
            maxDigit++;
        }
        int mod = 10, div = 1;
        ArrayList<List<Integer>> bucketList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            bucketList.add(new ArrayList<>());
        }
        for (int i = 0; i < maxDigit; i++, mod *= 10, div *= 10) {
            for (int j = 0; j < array.length; j++) {
                int num = (array[j] % mod) / div;
                bucketList.get(num).add(array[j]);
            }
            int index = 0;
            for (int j = 0; j < bucketList.size(); j++) {
                for (int k = 0; k < bucketList.get(j).size(); k++) {
                    array[index++] = bucketList.get(j).get(k);
                }
                bucketList.get(j).clear();
            }
        }
        return array;
    }


}
