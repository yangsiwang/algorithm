package com.mlamp.排序算法;

import java.util.Arrays;

public class 计数排序 {
    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        int[] ints1 = countSort(ints);
        System.out.println(Arrays.toString(ints1));
    }

    public static int[] countSort(int[] array) {
        int max = array[0], min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }
        int[] result = new int[max - min + 1];
        Arrays.fill(result, 0);
        for (int i = 0; i < array.length; i++) {
            result[array[i] - min]++;
        }
        int index = 0;
        for (int i = 0; i < result.length; i++) {
            if (result[i] != 0) {
                while (result[i] != 0) {
                    array[index++] = i + min;
                    result[i]--;
                }
            }
        }
        return array;
    }
}
