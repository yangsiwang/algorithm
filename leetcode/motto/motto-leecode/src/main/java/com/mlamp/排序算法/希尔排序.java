package com.mlamp.排序算法;

import java.util.Arrays;

public class 希尔排序 {

    public static void main(String[] args) {
        int[] ints = 基础排序.generateArray(30);
        shellSort(ints);
        System.out.println(Arrays.toString(ints));
    }

    public static void shellSort(int[] array) {
        int length = array.length;
        int gap = length / 2, temp;
        while (gap > 0) {
            for (int i = gap; i < length; i++) {
                temp = array[i];
                int preIndex = i - gap;
                while (preIndex >= 0 && temp < array[preIndex]) {
                    array[preIndex + gap] = array[preIndex];
                    preIndex -= gap;
                }
                array[preIndex + gap] = temp;
            }
            gap = gap / 2;
        }
    }
}
