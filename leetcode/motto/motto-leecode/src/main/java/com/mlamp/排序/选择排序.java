package com.mlamp.排序;

public class 选择排序 {
    public static void main(String[] args) {

    }

    public static int[] selectOnSort(int[] input) {
        if (input.length == 0) return input;
        for (int index = 0; index < input.length; index++) {
            int minIndex = index;
            for (int j = index; j < input.length; j++) {
                if (input[j] < minIndex) {
                    minIndex = j;
                }
            }
            int temp = input[minIndex];
            input[minIndex] = input[index];
            input[index] = temp;
        }
        return input;
    }
}
