package com.mlamp.排序;

public class 插入排序 {
    public static void main(String[] args) {

    }

    public static int[] insertOnSort(int[] array) {
        if (array.length == 0) return array;
        int current;
        for (int index = 0; index < array.length - 1; index++) {
            current = array[index + 1];
            int preIndex = index;
            while (preIndex >= 0 && current < array[preIndex]) {
                array[preIndex + 1] = array[preIndex];
                preIndex--;
            }
            array[preIndex + 1] = current;
        }
        return array;
    }
}
