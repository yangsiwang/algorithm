package com.mlamp.排序;

import java.util.Random;

public abstract class 排序 {

    public int[] randomIntArray(int length, int from, int to) {
        if (length <= 0) return new int[0];
        if (to < from) return new int[0];


        Random random = new Random();

        int result[] = new int[length];
        //   int count = length;
        for (int index = 0; index < length; index++) {
            int randomNumber = from + random.nextInt(to - from);
            result[index] = randomNumber;
        }
        return result;
    }
}
