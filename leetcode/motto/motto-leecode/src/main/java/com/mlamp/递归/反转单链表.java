package com.mlamp.递归;

public class 反转单链表 {

    public static void main(String[] args) {
        反转单链表 instance = new 反转单链表();
        Node curNode = null;
        Node head = null;
        for (int index = 0; index < 4; index++) {
            Node node = new Node();
            node.data = index + 1;
            if (curNode == null) {
                curNode = node;
                head = curNode;
            } else {
                curNode.next = node;
                curNode = node;
            }
        }
        /*while (head != null){
            System.out.println(head.data);
            head = head.next;
        }*/

        Node node = instance.reverseNode(head);

        Node tmp = node;
        while (tmp != null) {
            System.out.println(tmp.data);
            tmp = tmp.next;
        }
    }

    public static final class Node {
        int data;
        Node next;
    }

    /**
     * 1->2->3->4
     * reverseNode(2 todo reversenode(3 todo reverseNode(4 todo)))
     *
     * @param head
     * @return
     */
    public Node reverseNode(Node head) {
        if (head == null || head.next == null) return head;
        Node node = reverseNode(head.next);
        Node t1 = head.next;
        t1.next = head;
        head.next = null;
        return node;
    }


    public Node reverseNode3(Node head) {
        if (head == null || head.next == null) return head;
        Node node = reverseNode3(head.next);
        head.next.next = head;
        head.next = null;
        return node;
    }

    public Node reveresNode4(Node head) {
        if (head == null || head.next == null) return head;
        Node node = reveresNode4(head.next);
        head.next.next = head;
        head.next = null;
        return node;
    }

    public Node reverseNode5(Node head) {
        if (head == null || head.next == null) return head;
        Node node = reverseNode5(head.next);
        head.next.next = head;
        head.next = null;
        return node;
    }


    public Node reverseNode2(Node head) {
        if (head == null || head.next == null) return head;
        Node node = reverseNode2(head.next);
        head.next.next = head;
        head.next = null;
        return node;
    }

    /**
     * 反转单链表
     */

    public Node reverseNodeA(Node head) {
        if (head == null || head.next == null) return head;
        Node tmp = head.next;
        Node node = reverseNodeA(head.next);
        tmp.next = head;
        head.next = null;
        return node;
    }


    public Node reverseNodeCursiveA(Node head) {
        if (head == null || head.next == null) return head;
        Node temp = head.next;
        Node node = reverseNodeCursiveA(head.next);
        temp.next = head;
        head.next = null;
        return node;
    }


    public Node reverseNodeCursive(Node head) {
        if (head == null || head.next == null) return head;
        Node tmp = head.next;
        Node node = reverseNodeCursive(head.next);
        tmp.next = head;
        head.next = null;
        return node;
    }

    /**
     * 非递归反转单链表1->2->3->4
     *
     * @param node
     * @return
     */
    public static Node reverseLinkedList(Node node) {
        Node pre = null, next = null;
        while (node != null) {
            next = node.next;
            node.next = pre;
            pre = node;
            node = next;
        }
        return pre;
    }

    public static Node reverseC(Node node) {
        Node pre = null, next = null;
        while (node != null) {
            //next指向当前节点的下一个节点
            next = node.next;
            //当前节点的下一个节点指向前驱节点
            node.next = pre;
            //更新前驱节点
            pre = node;
            //更新后继节点
            node = next;
        }
        return pre;
    }


    public static Node reverseNodeC(Node node) {
        Node pre = null, next = null;
        while (node != null) {
            next = node.next;
            node.next = pre;
            pre = node;
            node = next;
        }
        return pre;
    }

    public static Node reverseLinkedListB(Node node) {
        Node pre = null, next = null;
        while (node != null) {
            next = node.next;
            node.next = pre;
            pre = node;
            node = next;
        }
        return pre;
    }


}
