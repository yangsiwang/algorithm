package com.mlamp.排序;

public class ShellOnSort {
    public static void main(String[] args) {

    }

    public static int[] shellOnSort(int[] array) {
        int len = array.length;
        int temp, gap = len / 2;
        while (gap > 0) {
            for (int index = gap; index < len; index++) {
                temp = array[index];
                int preIndex = index - gap;
                while (preIndex >= 0 && array[preIndex] > temp) {
                    array[preIndex + gap] = array[preIndex];
                    preIndex -= gap;
                }
                array[preIndex + gap] = temp;
            }
            gap /= 2;
        }
        return array;
    }
}
