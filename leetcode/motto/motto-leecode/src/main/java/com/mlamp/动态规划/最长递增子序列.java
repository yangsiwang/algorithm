package com.mlamp.动态规划;

public class 最长递增子序列 {
    public static void main(String[] args) {
        int[] array = new int[]{
                10, 9, 2, 5, 3, 7, 101, 18
        };
        int i = lengthOfLIS(array);
        System.out.println(i);
        i = lenOfLIS(array);
        System.out.println(i);
    }

    public static int lenOfLIS(int[] nums) {
        int res = 0;
        int dp[] = new int[nums.length];
        dp[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(dp[i], res);
        }
        return res;
    }

    //
    //
    public static int lengthOfLIS(int[] nums) {
        int dp[] = new int[nums.length];
        dp[0] = 1;
        int maxans = 1;
        for (int i = 1; i < nums.length; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            maxans = Math.max(maxans, dp[i]);
        }
        return maxans;
    }
}
