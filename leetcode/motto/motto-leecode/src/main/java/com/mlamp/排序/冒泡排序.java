package com.mlamp.排序;

import java.util.Arrays;

public class 冒泡排序 extends 排序 {

    public static void main(String[] args) {
        冒泡排序 instance = new 冒泡排序();
        int array[] = new int[]{
                2, 5, 8, 1, 3,100
        };

        int[] ints = instance.randomIntArray(100, 20, 50);
        System.out.println(Arrays.toString(ints));


        instance.bubbuleSortAsc(ints);
        instance.bubbuleSortDesc(ints);
    }

    public void bubbuleSortAsc(int array[]) {
        if (array.length < 1) return;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] >= array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }


    public void bubbuleSortDesc(int array[]) {
        if (array.length < 1) return;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] <= array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
