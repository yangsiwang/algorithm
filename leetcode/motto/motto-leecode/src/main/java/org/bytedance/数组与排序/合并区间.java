/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:合并区间.java
 * Date:2021/02/18 14:18:18
 */

package org.bytedance.数组与排序;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。
 * 请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。
 * <p>
 * 输入： intervals = [[1,3],[2,6],[8,10],[15,18]]
 * 输出：[[1,6],[8,10],[15,18]]
 * 解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6]
 * <p>
 * <p>
 * 输入：intervals = [[1,4],[4,5]]
 * 输出：[[1,5]]
 * 解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。
 */
public class 合并区间 {
    public static void main(String[] args) {
        int[][] inputs = new int[][]{
                new int[]{1, 3},
                new int[]{2, 6},
                new int[]{8, 10},
                new int[]{15, 18}
        };
        int[][] merge = merge(inputs);
        System.out.println(JSONObject.toJSONString(merge));

        inputs = new int[][]{
                new int[]{1, 4},
                new int[]{4, 5}
        };
        merge = merge(inputs);
        System.out.println(JSONObject.toJSONString(merge));
    }

    private static int[][] merge(int[][] intervals) {
        assert intervals != null && intervals.length > 1 : "invalid input param";
        Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));
        List<int[]> res = new ArrayList<>();
        res.add(intervals[0]);
        int last = 0;
        for (int i = 1; i < intervals.length; i++) {
            int lastStart = res.get(last)[0];
            int lastEnd = res.get(last)[1];
            int start = intervals[i][0];
            int end = intervals[i][1];
            if (start > lastEnd) {
                res.add(new int[]{start, end});
                last++;
            } else {
                res.set(last, new int[]{
                        Math.min(lastStart, start),
                        Math.max(end, lastEnd)
                });
            }
            /*//如果左边重合
            if (lastStart == start) {
                //如果当前结束大于之前的结束
                if (end > lastEnd) {
                    res.set(last, new int[]{start, end});
                } *//*else {
                    res.set(last, new int[]{start, lastEnd});
                }*//*
            } else {
                //如果当前的开始大于上一个的结束，则分属于两个区间
                if (start > lastEnd) {
                    res.add(new int[]{start, end});
                    last++;
                } else {
                    //如果当前的开始小于上一个的结束
                    if (end > lastEnd) {
                        res.set(last, new int[]{lastStart, end});
                    }
                }
            }*/
        }
        return res.toArray(new int[0][]);
    }
}
