/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:三数之和.java
 * Date:2021/02/18 14:17:18
 */

package org.bytedance.数组与排序;

import com.alibaba.fastjson.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有和为 0 且不重复的三元组。
 * <p>
 * [-1,0,1,2,-1,-4]
 * [[-1,-1,2],[-1,0,1]]
 */
public class 三数之和 {


    private static Set<List<Integer>> res = new HashSet<>();


    public static void main(String[] args) {
        int[] nums = new int[]{-1, 0, 1, 2, -1, -4};
        int sum = 0, size = 3;
        core(nums, sum, size);
        Set<List<Integer>> collect = res.stream().distinct().collect(Collectors.toSet());
        System.out.println(JSONObject.toJSONString(collect));

        // nums = []
        nums = new int[0];
        res.clear();
        core(nums, sum, size);
        collect = res.stream().distinct().collect(Collectors.toSet());
        System.out.println(JSONObject.toJSONString(collect));


        //nums = [0]
        res.clear();
        nums = new int[]{0};
        core(nums, sum, size);
        collect = res.stream().distinct().collect(Collectors.toSet());
        System.out.println(JSONObject.toJSONString(collect));

    }

    public static void core(int[] nums, int target, int size) {
        LinkedList<Integer> trace = new LinkedList<>();
        tripleSum(nums, target, trace, size, 0);
    }


    // 思路分析：决策树，回溯法
    private static void tripleSum(int[] nums, int target, LinkedList<Integer> trace, int size, int start) {
        if (sum(trace, nums) == target && trace.size() == size) {
            res.add(new ArrayList<>(trace.stream().map(item -> nums[item]).sorted().collect(Collectors.toList())));
            return;
        }
        for (int i = start; i < nums.length; i++) {
            if (trace.contains(i)) {
                continue;
            }
            trace.add(i);
            tripleSum(nums, target, trace, size, i);
            trace.removeLast();
        }
    }

    private static int sum(List<Integer> nums, int[] inputs) {
        int sum = 0;
        for (int item : nums) {
            sum += inputs[item];
        }
        return sum;
    }


    private static int sum(int... nums) {
        int sum = 0;
        for (int item : nums) {
            sum += item;
        }
        return sum;
    }


}
