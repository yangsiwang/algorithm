/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:俄罗斯套娃信封问题.java
 * Date:2021/02/18 14:22:18
 */

package org.bytedance.动态和贪心;

import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.bcel.internal.generic.LNEG;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 给定一些标记了宽度和高度的信封，宽度和高度以整数对形式 (w, h) 出现。当另一个信封的宽度和高度都比这个信封大的时候，这个信封就可以放进另一个信封里，如同俄罗斯套娃一样。
 * <p>
 * 请计算最多能有多少个信封能组成一组“俄罗斯套娃”信封（即可以把一个信封放到另一个信封里面）。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/russian-doll-envelopes
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 俄罗斯套娃信封问题 {


    public static void main(String[] args) {
        俄罗斯套娃信封问题 instance = new 俄罗斯套娃信封问题();
        int[][] envelopes = new int[][]{
                new int[]{5, 4},
                new int[]{6, 4},
                new int[]{6, 7},
                new int[]{2, 3}
        };


        int i = instance.maxEnvelopes(envelopes);
        System.out.println(i);

        envelopes = new int[][]{
                new int[]{1, 5},
                new int[]{1, 4},
                new int[]{1, 3},
                new int[]{1, 2},
                new int[]{2, 3}
        };
        i = instance.maxEnvelopes(envelopes);
        System.out.println(i);

        envelopes = new int[][]{
                new int[]{1,3},
                new int[]{3,5},
                new int[]{6,7},
                new int[]{6,8},
                new int[]{8,4},
                new int[]{9,5}
        };
        i = instance.maxEnvelopes(envelopes);
        System.out.println(i);

    }


    /**
     * 排序 + 最长递增子序列
     *
     * @param envelopes
     * @return
     */
    public int maxEnvelopes(int[][] envelopes) {
        if (envelopes == null || envelopes.length == 0) return 0;
        Arrays.sort(envelopes, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if (o1[0] == o2[0]) {
                    return o1[1] - o2[1];
                } else {
                    return o1[0] - o2[0];
                }
            }
        });
       /* int[] dp = new int[envelopes.length];
        for (int i = 0; i < envelopes.length; i++) {
            dp[i] = envelopes[i][1];
        }

        return lengthOfLIS(dp);*/

        int[] dp = new int[envelopes.length];
        dp[0] = 1;
        int res = 1;
        for (int i = 1; i < envelopes.length; i++) {
            for (int j = 0; j < i; j++) {
                if (envelopes[i][0] > envelopes[j][0] && envelopes[i][1] > envelopes[j][1]){
                    dp[i] =  Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        System.out.println(JSONObject.toJSONString(envelopes));
        System.out.println(JSONObject.toJSONString(dp));
        return res;


    }

    private int lengthOfLIS(int[] nums) {
        int[] dp = new int[nums.length];
        int len = 0;
        for (int num : nums) {
            int i = Arrays.binarySearch(dp, 0, len, num);
            if (i < 0) {
                i = -(i + 1);
            }
            dp[i] = num;
            if (i == len) {
                len++;
            }
        }
        return len;
    }

}
