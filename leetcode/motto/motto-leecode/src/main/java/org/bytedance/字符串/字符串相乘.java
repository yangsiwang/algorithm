/*
 * Copyright (c) 2021
 * User:LENOVO
 * File:字符串相乘.java
 * Date:2021/02/18 14:15:18
 */

package org.bytedance.字符串;

import java.math.BigInteger;
import java.util.regex.Pattern;

/**
 * 给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。
 * <p>
 * 输入: num1 = "2", num2 = "3"
 * 输出: "6"
 * <p>
 * 输入: num1 = "123", num2 = "456"
 * 输出: "56088"
 */
public class 字符串相乘 {

    public static void main(String[] args) {
        字符串相乘 instance = new 字符串相乘();
        String num1 = "02", num2 = "3";
        String res = instance.multiply(num1, num2);
        System.out.println(res);

        num1 = "1234";
        num2 = "567";
        res = instance.multiply(num1, num2);
        System.out.println(res);

        num1 = "123";
        num2 = "456";
        res = instance.multiply(num1, num2);
        System.out.println(res);

        num1 = "123456789234567890";
        num2 = "234567890234567890";
        res = instance.multiply(num1, num2);
        System.out.println(res);


        num1 = "123";
        num2 = "456";
        res = instance.multiply2(num1, num2);
        System.out.println(res);

        num1 = "1234";
        num2 = "567";
        res = instance.multiply2(num1, num2);
        System.out.println(res);

        num1 = "123456789234567890";
        num2 = "234567890234567890";
        BigInteger bigInteger = new BigInteger(num1);
        BigInteger bigInteger1 = new BigInteger(num2);
        System.out.println(bigInteger.multiply(bigInteger1));

        System.out.println(instance.multiply2(num1, num2));
        System.out.println(instance.multiply3(num1, num2));

    }


    public String multiply2(String num1, String num2) {
        if (num1 == null || num2 == null || !(Pattern.compile("^\\d+$").matcher(num1).find()) ||
                !(Pattern.compile("^\\d+$").matcher(num2).find())) throw new IllegalArgumentException("bad param");
        if (num1.isEmpty() || num2.isEmpty()) throw new IllegalArgumentException("bad param");
        if (num1.equals("0") || num2.equals("0")) return "0";
        int m = num1.length(), n = num2.length();
        String res = "";
        for (int i = n - 1; i >= 0; i--) {
            int carry = 0;
            String tmp = "";
            for (int j = 0; j < n - 1 - i; j++) {
                tmp += "0";
            }
            int x = num2.charAt(i) - '0';
            for (int j = m - 1; j >= 0 || carry != 0; j--) {
                int y = j < 0 ? 0 : num1.charAt(j) - '0';
                int product = (x * y + carry) % 10;
                carry = (x * y + carry) / 10;
                tmp = product + tmp;
            }
            res = addString(res, tmp);
        }

        return res;
    }

    private String addString(String num1, String num2) {
        String res = "";
        int carry = 0;
        for (int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0 || carry != 0; i--, j--) {
            int x = i < 0 ? 0 : num1.charAt(i) - '0';
            int y = j < 0 ? 0 : num2.charAt(j) - '0';
            int sum = (x + y + carry) % 10;
            res = sum + res;
            carry = (x + y + carry) / 10;
        }
        return res;
    }


    public String multiply(String num1, String num2) {
        if (num1 == null || num2 == null || !(Pattern.compile("^\\d+$").matcher(num1).find()) ||
                !(Pattern.compile("^\\d+$").matcher(num2).find())) throw new IllegalArgumentException("bad param");
        if (num1.isEmpty() || num2.isEmpty()) throw new IllegalArgumentException("bad param");
        if (num1.equals("0") || num2.equals("0")) return "0";
        int m = num1.length(), n = num2.length();
        int[] res = new int[m + n];
        for (int i = m - 1; i >= 0; i--) {
            int x = num1.charAt(i) - '0';
            for (int j = n - 1; j >= 0; j--) {
                int y = num2.charAt(j) - '0';
                int sum = (res[i + j + 1] + x * y);
                res[i + j + 1] = sum % 10;
                res[i + j] = sum / 10;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < res.length; i++) {
            if (i == 0 && res[i] == 0) continue;
            sb.append(res[i]);
        }
        return sb.toString();
    }


    /**
     * 计算形式
     * num1
     * x num2
     * ------
     * result
     */
    public String multiply3(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        // 保存计算结果
        String res = "0";

        // num2 逐位与 num1 相乘
        for (int i = num2.length() - 1; i >= 0; i--) {
            int carry = 0;
            // 保存 num2 第i位数字与 num1 相乘的结果
            StringBuilder temp = new StringBuilder();
            // 补 0
            for (int j = 0; j < num2.length() - 1 - i; j++) {
                temp.append(0);
            }
            int n2 = num2.charAt(i) - '0';

            // num2 的第 i 位数字 n2 与 num1 相乘
            for (int j = num1.length() - 1; j >= 0 || carry != 0; j--) {
                int n1 = j < 0 ? 0 : num1.charAt(j) - '0';
                int product = (n1 * n2 + carry) % 10;
                temp.append(product);
                carry = (n1 * n2 + carry) / 10;
            }
            // 将当前结果与新计算的结果求和作为新的结果
            res = addStrings(res, temp.reverse().toString());
        }
        return res;
    }

    /**
     * 对两个字符串数字进行相加，返回字符串形式的和
     */
    public String addStrings(String num1, String num2) {
        StringBuilder builder = new StringBuilder();
        int carry = 0;
        for (int i = num1.length() - 1, j = num2.length() - 1;
             i >= 0 || j >= 0 || carry != 0;
             i--, j--) {
            int x = i < 0 ? 0 : num1.charAt(i) - '0';
            int y = j < 0 ? 0 : num2.charAt(j) - '0';
            int sum = (x + y + carry) % 10;
            builder.append(sum);
            carry = (x + y + carry) / 10;
        }
        return builder.reverse().toString();
    }
}
