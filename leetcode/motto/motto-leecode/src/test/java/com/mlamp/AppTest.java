package com.mlamp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void test1(){
        String[] inputs = new String[]{
                "25525511135",
                "0000",
                "1111",
                "010010",
                "101023",
                "232332@ewe"
        };
        Pattern pattern = Pattern.compile("^\\d{4,12}$");
        for (String line : inputs){
            Matcher matcher = pattern.matcher(line);
            if (!matcher.find()) {
                System.out.println("not found " + line);
            } else {
                System.out.println("find one " + line);
            }
        }

    }
}
